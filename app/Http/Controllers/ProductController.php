<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::all();
        return $products;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $products= new Product();
        $products->userRent_id=$request->userRent_id;
        $products->name_product=$request->name_product;
        $products->quantity=$request->quantity;
        $products->pricerent=$request->pricerent;

        $products->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {    $products= Product::findOrFail($request->id);
        $products->userRent_id=$request->userRent_id;
        $products->name_product=$request->name_product;
        $products->quantity=$request->quantity;
        $products->pricerent=$request->pricerent;
        $products->save();
        return $products;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $products= Product::destroy($request->id);
        return $products;
    }
    public function showbyid(Request $request){
        if($request->id!=null){
        $products=Product::find($request->id);
        }
        
        return $products;
    }
   

}
