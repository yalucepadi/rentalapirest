<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Usersrent;
use Auth;

class UsersrentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usersrent=UsersRent::all();
        return $usersrent;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usersrent= new UsersRent();
        $usersrent->username=$request->username;
        $usersrent->password=$request->password;
        $usersrent->district=$request->district;
        $usersrent->abbreviations=$request->abbreviations;
        $usersrent->address=$request->address;
        $usersrent->save();

           return $usersrent;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $usersrent= UsersRent::findOrFail($request->id);
        $usersrent->username=$request->username;
        $usersrent->password=$request->password;
        $usersrent->district=$request->district;
        $usersrent->abbreviations=$request->abbreviations;
        $usersrent->address=$request->address;
        $usersrent->save();
        return $usersrent;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $usersrent= UsersRent::destroy($request->id);
        return $usersrent;
    }
    public function showbyid(Request $request){
        if($request->id!=null){
        $usersrent=UsersRent::find($request->id);
        }
        
        return $usersrent;
    }
    public function search($username,$password){

     $result=DB::select("SELECT id, username,district,abbreviations,address,created_at,updated_at FROM usersrents WHERE  username='" .$username. "' and password='" .$password."'");  
    if(sizeof($result)==0){
        return  '{"result":"bad login"}';
    }
    else{
        return '{"result":"login success"}';
    }
     
    }
    public function showbyuserName($username,$password){
        
        $usersrent=DB::select("SELECT id, username,district,abbreviations,address,created_at,updated_at FROM usersrents WHERE  username='" .$username. "' and password='" .$password."'"); 
        
        
            return $usersrent;
        
        
        
    }

}
