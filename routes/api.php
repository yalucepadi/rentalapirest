<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::get('/userRent','App\Http\Controllers\UsersrentController@index');//mostrar todos los registros
Route::get('/userRent/{id}','App\Http\Controllers\UsersrentController@showbyid');//mostrar registros por id
Route::post('/userRent/crear','App\Http\Controllers\UsersrentController@store');//crear un registro
Route::put('/userRent/actualizar/{id}','App\Http\Controllers\UsersrentController@update');//actualizar un registro
Route::delete('/userRent/eliminar/{id}','App\Http\Controllers\UsersrentController@destroy');//eliminar un registro
Route::get('/userRent/login/{username}/{password}','App\Http\Controllers\UsersrentController@search');//mostrar estado Login
Route::get('/userRent/dataUser/{username}/{password}','App\Http\Controllers\UsersrentController@showbyuserName');//mostrar usuario por username

Route::get('/product','App\Http\Controllers\ProductController@index');//mostrar todos los registros
Route::get('/product/{id}','App\Http\Controllers\ProductController@showbyid');//mostrar registros por id
Route::post('/product/crear','App\Http\Controllers\ProductController@store');//crear un registro
Route::put('/product/actualizar/{id}','App\Http\Controllers\ProductController@update');//actualizar un registro
Route::delete('/product/eliminar/{id}','App\Http\Controllers\ProductController@destroy');//eliminar un registro